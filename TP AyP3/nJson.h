#ifndef __NJSON_H__
#define __NJSON_H__

#include <stdbool.h>

// El tipo que se usará para las funciones de escritura.
typedef void* Escribir(FILE*, void*);

/*
 * Detalle del encabezado (Guarda datos del JSon internos)
 */
typedef struct _nJson {
    void* valor;
    char* nombre;
    struct _nJson* proximo;
    bool esArray;
    unsigned valueSize; // tamaño del elemento
	Escribir* func;
} nJson;

/*
 * Nuevo Encabezado para maneajar los datos internos
 */
typedef struct _JSon {
	nJson* njson;
    char* nombre;
} JSon;

/* Inicializa JSon con nombre y primer dato dentro del JSon */
JSon* nJson_init(JSon* this, const char* nombre, const char* nombreDato, void* valor, unsigned tamanioDato, unsigned cantidad, bool esArray, Escribir func);

/* Inicializa el nJson*/
void nJson_initnJson (JSon* this, const char* nombreDato, void* valor, unsigned tamanioDato, unsigned cantidad, bool esArray, Escribir func);

/* Verifico si el dato ya existe con ese nombre
 * Si this->name == name devuelveel mismo,sino pasa al siguiente dato buscando recursivamente hasta finalizar los datos.*/
nJson* nJson_tieneDato (nJson* this, const char* nombreDato);

/* Agrega un dato al nJson recursivamente mediante lista
(si this == 0x0, crea el dato, sino pasa al que sigue). */
nJson* nJson_agregarDato (nJson* this, const char* nombreDato, void* valor, unsigned tamanioDato, unsigned cantidad, bool esArray, Escribir func);

void nJson_modificarDato(JSon* this, const char* nombreDato, void* valor, unsigned tamanioDato, unsigned cantidad, bool esArray, Escribir func);

void nJson_release(JSon* this);

//Libero todos los nJson asignado al JSON
void nJson_liberarDato(nJson* this);

void nJson_finalizaImpresion(FILE* file);

void* nJson_imprimirJSon(FILE* archivo, void* valor);

void nJson_imprimirDato (FILE* file, JSon* this, const char* nombreDato);

void* nJson_imprimir_entero(FILE* file, void* valor);

void* nJson_imprimir_sin_signo(FILE* file, void* valor);

void* nJson_imprimir_decimal(FILE* file, void* valor);

void* nJson_imprimir_flotante(FILE* file, void* valor);

void* nJson_imprimir_flotante_corto(FILE* file, void* valor);

void* nJson_imprimir_string(FILE* file, void* valor);

void* nJson_imprimir_booleano(FILE* file, void* valor);

#endif
