#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "nJson.h"

int main(int argc, char** argv) {
	// Captura si escribo por consola o por archivo
	FILE* file = 0x0;
	if (argc == 3 && !strcmp(argv[1], "-f")){
	   	//Puntero a Archivo
	   	file = fopen(argv[2], "w+");
	}
	else if (argc == 3 && !strcmp(argv[1], "-c")){
	   	//Crear otro JSON y mostrarlo en la salida utlizando el parámetro -C
		file = stdout;

		JSon* json1 = NULL;
		char* tamanio = "treinta";
		char* hash = "3DFMAP0932C";
		unsigned bytes = 8;
		bool thumb_exists = true;

		json1 = nJson_init (json1, "jSon1", "tamaño", tamanio, sizeof(tamanio)+1, true, 0, nJson_imprimir_string);
		nJson_initnJson(json1, "hash", hash, sizeof(hash)+1, true, 0, nJson_imprimir_string);
		nJson_initnJson(json1, "bytes", &bytes, sizeof(unsigned), true, 0, nJson_imprimir_sin_signo);
		nJson_initnJson(json1,"thumb_exists", &thumb_exists, sizeof(bool), true, 0, nJson_imprimir_booleano);

		nJson_imprimirJSon(file, json1);
		nJson_finalizaImpresion(file);
		nJson_release(json1);
		exit(0); //salgo de la ejecucion, solo muestro en la salida el json1

	}
	else{
	   	//Puntero que permite salida x consola
	   	file = stdout;
	}

	char* size = "0 bytes";
	char* hash = "37eb1ba1849d4b0fb0b28caf7ef3af52";
	unsigned bytes = 0;
	bool thumb_exists = false;
	char* rev = "714f029684fe";
	char* modified = "Wed, 27 Apr 2011 22:18:51 +0000";
	char* path = "/Photos";
	bool is_dir= true;
	char* icon= "folder";
	char* root = "dropbox";
	//int  contents= 1;
	//int array[3]= {1,2,3};
	JSon* json =NULL;
	json = nJson_init(json,"JSON","size",size, strlen(size)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(json, "hash", hash, strlen(hash)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(json, "bytes", &bytes, sizeof(unsigned), 1, false, nJson_imprimir_sin_signo);
	nJson_initnJson(json, "thumb_exists", &thumb_exists, sizeof(bool), 1, false, nJson_imprimir_booleano);
	nJson_initnJson(json, "rev", rev, strlen(rev)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(json, "modified", modified, strlen(modified)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(json, "path", path, strlen(path)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(json, "is_dir", &is_dir, sizeof(bool), 1, false, nJson_imprimir_booleano);
	nJson_initnJson(json, "icon", icon, strlen(icon)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(json, "root", root, strlen(root)+1, 1, false, nJson_imprimir_string);
	//nJson_initnJson(json, "contents", &contents, sizeof(int), 1, false, nJson_imprimir_entero);
	//nJson_initnJson(json, "array", array, sizeof(array), sizeof(array)/sizeof(int), true, nJson_imprimir_entero);


	JSon* contents=NULL;

	char* size1 = "2.3 MB";
	char* rev1= "38af1b183490";
	bool thumb_exists1 = true;
	unsigned bytes1= 2453963;
	char* modified1 = "Mon, 07 Apr 2014 23:13:16 +0000";
	char* client_mtime = "Thu, 29 Aug 2013 01:12:02 +0000";
	char* path1 = "/Photos/flower.jpg";

	contents = nJson_init(contents, "contents", "size",size1, strlen(size1)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(contents, "rev", rev1, strlen(rev1)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(contents, "thumb_exists", &thumb_exists1, sizeof(bool), 1, false, nJson_imprimir_booleano);
	nJson_initnJson(contents, "bytes", &bytes1, sizeof(unsigned), 1, false, nJson_imprimir_sin_signo);
	nJson_initnJson(contents, "modified", modified1, strlen(modified1)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(contents, "client_mtime", client_mtime, strlen(client_mtime)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(contents, "path", path1, strlen(path1)+1, 1, false, nJson_imprimir_string);

	JSon* photo_info=NULL;

	double lat_long[2] = {37.77256666666666, -122.45934166666667};

	photo_info = nJson_init(photo_info, "photo_info", "lat_long", lat_long, sizeof(lat_long), sizeof(lat_long)/sizeof(double), true, nJson_imprimir_decimal);
	char* time_taken= "Wed, 28 Aug 2013 18:12:02 +0000";

	nJson_initnJson(photo_info, "time_taken", time_taken, strlen(time_taken)+1, 1, false, nJson_imprimir_string);

	//Agrego el json photo_info dentro de contents
	nJson_initnJson(contents, "photo_info", photo_info, sizeof(photo_info), 1, false, nJson_imprimirJSon);

	bool is_dir1 = false;
	char* icon1= "page_white_picture";
	char* root1 = "dropbox";
	char* mime_type= "image/jpeg";
	unsigned revision =  14511;

	nJson_initnJson(contents, "is_dir", &is_dir1, sizeof(bool), 1, false, nJson_imprimir_booleano);
	nJson_initnJson(contents, "icon1", icon1, strlen(icon1)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(contents, "root", root1, strlen(root1)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(contents, "mime_type", mime_type, strlen(mime_type)+1, 1, false, nJson_imprimir_string);
	nJson_initnJson(contents, "revision", &revision, sizeof(unsigned), 1, false, nJson_imprimir_sin_signo);

	nJson_initnJson(json, "contents", contents, sizeof(contents), 1, false, nJson_imprimirJSon);

	unsigned revision2 = 29007;
	nJson_initnJson(json, "revision", &revision2, sizeof(unsigned), 1, false, nJson_imprimir_sin_signo);

    nJson_imprimirJSon(file, json);
    nJson_finalizaImpresion(file);
    nJson_release(photo_info);
    nJson_release(contents);
    nJson_release(json);

    return 0;
}
