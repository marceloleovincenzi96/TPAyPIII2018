#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "nJson.h"

JSon* nJson_init(JSon* this, const char* nombre, const char* nombreDato, void* valor, unsigned tamanioDato, unsigned cantidad, bool esArray, Escribir func) {

	if (valor != 0x0){
		this = malloc (sizeof (nJson));
		this->nombre = malloc( sizeof(char) * (strlen(nombre) + 1) );
		strcpy(this->nombre, nombre);
		//Asignación x defecto
		this->njson = NULL;
		nJson_initnJson(this,nombreDato,valor,tamanioDato,cantidad,esArray,func);

		return this;
	}
	else {
		//TODO Informar error valor nulo
		return 0x0;
	}
}

// Inicializa el nJson ()
void nJson_initnJson(JSon* this, const char* nombreDato, void* valor, unsigned tamanioDato, unsigned cantidad, bool esArray, Escribir func){

	//Valido entradas this, nombreDato, valor, tamanioDato
	nJson* nJsonTemp = nJson_tieneDato(this->njson, nombreDato);
		if (!nJsonTemp){
			this->njson = nJson_agregarDato(this->njson, nombreDato, valor, tamanioDato, cantidad, esArray, func);
		}
		else
		{
			free (nJsonTemp->valor);
			nJsonTemp->valor = malloc(tamanioDato);
			memcpy(nJsonTemp->valor, valor, tamanioDato);
			nJsonTemp->esArray = esArray;
			nJsonTemp->valueSize = cantidad;
			nJsonTemp->func = func;
		}
}

/* Verifico si el dato ya existe con ese nombre
   Si this->name == name devuelve el mismo, sino pasa al siguiente dato buscando recursivamente hasta finalizar los datos.*/
nJson* nJson_tieneDato (nJson* this, const char* nombreDato) {
    if (this) {
    	if (strcmp(this->nombre, nombreDato)==0){
    		//mismo dato devuelvo el mismo
    		return this;
    	}
    	else
    	{
    		//Busqueda recursiva hasta encontrar el dato o llegar a no tener mas datos
    		return nJson_tieneDato (this->proximo, nombreDato);
    	}
    }
    else {
    	return NULL;
    }
}

/* Método auxiliar que permite para agregar un dato recursivamente sobre la lista
* (Si this == 0x0, creo dato, sino paso al siguiente) */
nJson* nJson_agregarDato (nJson* this, const char* nombreDato, void* valor, unsigned tamanioDato, unsigned cantidad, bool esArray, Escribir func){
	if (!this){
		//creo nuevo Dato
		this = malloc (sizeof(nJson));

		this->nombre = malloc (strlen (nombreDato)+1);
		strcpy (this->nombre, nombreDato);

		this->valor = malloc(tamanioDato);
		memcpy(this->valor, valor, tamanioDato);

		this->esArray = esArray;
		this->valueSize = cantidad;
		this->func = func;
		this->proximo = NULL;
	}
	else {
		//El dato ya existe paso al siguiente y lo agrego recursivamente
		this->proximo = nJson_agregarDato (this->proximo, nombreDato, valor, tamanioDato, cantidad, esArray, func);
	}
	return this;
}

void nJson_modificarDato(JSon* this, const char* nombreDato, void* valor, unsigned tamanioDato, unsigned cantidad, bool esArray, Escribir func){

	if (this) {
		if (nombreDato){
			if (tamanioDato)
			{
				//Traigo dato (si existe)
				nJson* nJsontemp = nJson_tieneDato(this->njson, nombreDato);
				if (nJsontemp) {
				    free (nJsontemp->valor);
				    nJsontemp->valor = malloc(tamanioDato);
				    memcpy(nJsontemp->valor, valor, tamanioDato);
				    nJsontemp->esArray = esArray;
				    nJsontemp->valueSize = cantidad;
				    nJsontemp->func = func;
				}
				else {
				    this->njson = nJson_agregarDato(this->njson, nombreDato, valor, tamanioDato, cantidad, esArray, func);
				}
			}
			else{
				//TODO Error tamanio no definido
			}
		}
		else {
			//TODO Error puntero a nombreDato no existe
		}
	}
	else
	{
		//TODO Error puntero a JSON no existe;
	}
}

void nJson_release(JSon* this) {
	if (this){
		nJson_liberarDato(this->njson);
		this->njson = 0x0;
		if ( this->nombre) {
		        free( this->nombre );
		        this->nombre = 0x0;
		}
		free (this);
    }
}

//libero nJson
void nJson_liberarDato(nJson* this){
	if (this){
		if (this->nombre){
			free (this->nombre);
		}
	    if ( this->valor) {
	        free( this->valor );
	    }
	    this->nombre = 0x0;
	    this->valor = 0x0;
	    this->valueSize = 0x0;
	    this->esArray = 0x0;
	    this->func = 0x0;

	    //Libero todos los nJson
	    //TODO Esto deberia ser asi
	    nJson_liberarDato(this->proximo);
	    free(this);
	    this->proximo = 0x0;
		this = 0x0;
	}
}

void nJson_finalizaImpresion(FILE* file){
	fclose (file);
}

void nJson_imprimirDato (FILE* file, JSon* this, const char* nombreDato) {
	//TODO Validar puntero a file
	//TODO Validar puntero a JSon
    nJson* dato = nJson_tieneDato(this->njson, nombreDato);
    //TODO Valido dato traido
    fprintf (file, "\"%s\":", dato->nombre);
    if (dato->esArray) {
    	fprintf (file, "[");
    }
    void* temp = dato->valor;
    int i;
    for (i = 0; i < dato->valueSize; i++) {
    	temp = dato->func(file, temp);
    	if (i < (dato->valueSize)-1)
    	{
    		fprintf (file, ",");
    	}
    }
    if (dato->esArray) {
    	//nJson_eliminaUltimoCaracter(file);
    	fprintf (file, "]");
   }
}

void* nJson_imprimirJSon(FILE* file, void* this) {
    JSon* json = (JSon*)this;
    if (!json) {
    	exit(2);
   	}

    if (!file) {exit(3);}
    fprintf(file, "{");
    nJson* dato = json->njson;
    if (!dato){
    	exit(4);
    }
    //Aca recorro si es un Array cada njson (tipo for each)
    do {
    	nJson_imprimirDato(file, json, dato->nombre);
    	if (dato->proximo) {
    		fprintf(file, ",");
    	}
    } while ((dato = dato->proximo));

    fprintf (file, "}");

    return this += sizeof (nJson);
}

void* nJson_imprimir_entero (FILE* file, void* valor) {
    //TODO Verificar puntero a File
    //TODO Verificar Valor

    fprintf (file, "%d", *(int*)valor);
    return valor += sizeof (int);
}

void* nJson_imprimir_sin_signo (FILE* file, void* valor) {
	//TODO Verificar puntero a File
	//TODO Verificar Valor

    fprintf (file, "%u", *(unsigned*)valor);
    return valor += sizeof (unsigned);
}

void* nJson_imprimir_decimal(FILE* file, void* valor) {
	//TODO Verificar puntero a File
	//TODO Verificar Valor

    fprintf (file, "%f", *(double*)valor);
    return valor += sizeof (double);
}

void* nJson_imprimir_flotante(FILE* file, void* valor) {
	//TODO Verificar puntero a File
	//TODO Verificar Valor

    fprintf (file, "%f", *(float*)valor);
    return valor += sizeof (float);
}

void* nJson_imprimir_flotante_corto(FILE* file, void* valor) {
	//TODO Verificar puntero a File
	//TODO Verificar Valor

    fprintf (file, "%u", *(short*)valor);
    return valor += sizeof (short);
}

void* nJson_imprimir_flotante_largo(FILE* file, void* valor) {
	//TODO Verificar puntero a File
	//TODO Verificar Valor

	fprintf (file, "%ld", *(long*)valor);
    return valor += sizeof(long);
}

void* nJson_imprimir_string(FILE* file, void* valor){
	//TODO Verificar puntero a File
	//TODO Verificar Valor
	fprintf (file, "\"%s\"", (char*)valor);
	return valor+= strlen((char*)valor)+1;
}

void* nJson_imprimir_booleano(FILE* file, void* valor) {
	//TODO Verificar puntero a File
	//TODO Verificar Valor

	if(*(bool*)valor){
		fprintf(file, "true");
	}
	else {
		fprintf(file, "false");
	}
	return valor += sizeof (bool);
}
